FROM python:3.11-alpine
LABEL Author="Ashwin V. Mohanan <dev@fluid.quest>"

ENV PATH="/root/.local/bin:$PATH"

RUN apk add --no-cache curl ca-certificates git openssh curl git-lfs && \
    curl -sSLO https://pdm-project.org/install-pdm.py && \
    python3 install-pdm.py && \
    python3 -m venv /root/venv

COPY . /tmp/website
WORKDIR /root

# RUN --mount=type=bind,source=.,target=/tmp/website,z \
RUN pdm use -f /root/venv -p /tmp/website && \
    pdm install --no-editable -p /tmp/website && \
    python3 /install-pdm.py --remove && \
    rm -r /root/.cache /tmp/website

ENV PATH="/root/venv/bin:$PATH"

# ENTRYPOINT ["venv/bin/python"]
# CMD ["-m", "site"]
