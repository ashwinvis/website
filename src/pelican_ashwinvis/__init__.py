from pathlib import Path

try:
    import tomllib as toml
except ImportError:
    import tomli as toml


# Read some things defined in [tool.pelican-ashwinvis]
if (pyproject := Path("pyproject.toml")).exists():
    with pyproject.open("rb") as fp:
        _d = toml.load(fp)["tool"]["pelican-ashwinvis"]
else:
    from warnings import warn

    warn(f"No pyproject.toml found in {Path.cwd()}")
    _d = {}

SITEURL = _d.get("SITEURL")
FEDIHOST = _d.get("FEDIHOST")
FEDIAPI = _d.get("FEDIAPI")
FEDIUSER = _d.get("FEDIUSER")
FEDI_ID_PLACEHOLDER = _d.get("FEDI_ID_PLACEHOLDER")
