from . import write


def bridgy(slug, posse):
    """
    curl -i -d source=https://fluid.quest/hack-the-crisis.html \
            -d target=https://brid.gy/publish/mastodon \
            https://brid.gy/publish/webmention
    """
    data = {
        "source": write.post_url(slug),
        "target": "https://brid.gy/publish/" + posse,
        "endpoint": "https://brid.gy/publish/webmention",
    }
    #  headers = {"User-Agent": "Mozilla/4.0"}
    #  r = requests.post(
    #      "https://brid.gy/publish/webmention",
    #      headers=headers,
    #      data=payload,
    #      timeout=1,
    #  )
    #  r.raise_for_status()
    #  print(r.text)
    #  print("Status: ", r.status_code)
    # Requires: webmention-toold
    from webmentiontools.send import WebmentionSend

    mention = WebmentionSend(**data)
    mention.send()
    return mention
