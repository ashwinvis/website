from datetime import datetime

from textual.app import App, ComposeResult
from textual.containers import Grid, Horizontal
from textual.css.query import NoMatches
from textual.errors import TextualError
from textual.screen import ModalScreen
from textual.widgets import (
    Button,
    Digits,
    Header,
    Input,
    Label,
    RadioButton,
    RadioSet,
    Static,
    Switch,
)


class BlogNew(App):
    CSS_PATH = "blog_new.tcss"
    template_filetypes = ("md", "rst", "ipynb")

    def __init__(self):
        super().__init__()
        self.context = None

    def compose(self) -> ComposeResult:
        yield Header()
        yield Input(id="title", placeholder="Title")
        yield Input(id="summary", placeholder="Summary")
        with RadioSet(id="category"):
            yield RadioButton("Blog")
            yield RadioButton("Tech Talk")

        with RadioSet(id="tags", classes="horizontal"):
            yield RadioButton("life")
            yield RadioButton("research")
            yield RadioButton("software")

        with RadioSet(id="filetype", classes="horizontal"):
            yield from (RadioButton(ft) for ft in self.template_filetypes)

        yield Horizontal(
            Static("publish post: ", classes="label"),
            Switch(value=False, id="publish-status"),
            classes="container",
        )

        yield Horizontal(
            Button("Create!", id="button-create", variant="primary"),
            Static(" at ", classes="label"),
            Digits(id="date"),
        )

    def on_mount(self) -> None:
        """Updates Header."""
        self.title = "Blog"
        self.sub_title = "New"

    def on_ready(self) -> None:
        self.update_date()
        self.set_interval(1, self.update_date)

    def update_date(self) -> None:
        now = datetime.now().isoformat()
        try:
            self.query_one(Digits).update(now)
        except NoMatches:
            pass

    def _get_value(self, inp: Input):
        if value := inp.value:
            return value
        else:
            raise TextualError(f"Expected a value for {inp.id}")

    def _get_button(self, radio_set: RadioSet):
        if not (button := radio_set.pressed_button):
            raise TextualError(f"No button set for {radio_set =}")

        if not (label := button.label):
            raise TextualError(f"Expected a label for {button} with id {button.id}")

        return str(label)

    def _get_publish_status(self, switch: Switch):
        return "published" if switch.value else "draft"

    def on_button_pressed(self, event: Button.Pressed) -> None:
        if not event.button.id == "button-create":
            return

        try:
            title = self._get_value(self.query_one("#title", Input))
            self.context = {
                "title": title,
                "summary": self._get_value(self.query_one("#summary")),
                "slug": title.lower().replace(" ", "-")[:42],
                "tags": self._get_button(self.query_one("#tags", RadioSet)),
                "category": self._get_button(self.query_one("#category", RadioSet)),
                "filetype": self._get_button(self.query_one("#filetype", RadioSet)),
                "status": self._get_publish_status(
                    self.query_one("#publish-status", Switch)
                ),
                "date": self._get_value(self.query_one("#date", Digits)),
                "author": "Ashwin Vishnu Mohanan",
            }
        except (TextualError, NoMatches) as err:
            self.push_screen(ErrorScreen(str(err)))
        else:
            self.exit(str(event.button))


class ErrorScreen(ModalScreen[bool]):
    """Screen with a error popup."""

    def __init__(self, prompt: str):
        super().__init__()
        self.prompt = prompt

    def compose(self) -> ComposeResult:
        yield Grid(
            Label(self.prompt, id="prompt"),
            Button("Continue", variant="primary", id="button-continue"),
            id="dialog",
        )

    def on_button_pressed(self, event: Button.Pressed) -> None:
        if event.button.id == "button-continue":
            # self.dismiss(False)
            self.app.pop_screen()
        else:
            return


def run():
    app = BlogNew()
    app.run()
    print(app.context)
    return app


if __name__ == "__main__":
    run()
