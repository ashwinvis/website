#!/home/avmo/.pyenv/versions/www/bin/python
import os
import re
import shutil
import subprocess
import sys
from datetime import date
from pathlib import Path

# TODO: replace with typer
import click
from jinja2 import Environment, FileSystemLoader
from rich.prompt import Confirm

from pelican_ashwinvis import FEDI_ID_PLACEHOLDER, FEDIHOST, FEDIUSER, SITEURL


def edit(filename):
    editor = shutil.which(os.getenv("EDITOR", "vim"))
    subprocess.run([editor, filename])


here = Path(__file__).parent / ".." / ".." / ".."
click_kwargs = {"context_settings": dict(help_option_names=["-h", "--help"])}


@click.group(**click_kwargs)
def write():
    pass


@write.command(**click_kwargs)
@click.argument("files", nargs=-1, type=click.Path())
def modify(files):
    content = here / "content"
    if not files:
        all_files = [f for f in content.glob("*") if f.is_file()][::-1]
        for i, f in enumerate(all_files):
            print(i, ":", f.relative_to(content))

        ans = input("Enter file to edit: ")
        if ans.isdecimal():
            # as index of list
            ia = int(ans)
            files = all_files[ia : ia + 1]
        else:
            # as filename
            files = [ans]

    for filename in files:
        if not os.path.exists(filename):
            filename = content / filename

        edit(filename)
        git_prompt(filename)


def post_url(slug):
    return os.path.join(SITEURL, slug) + ".html"


@write.command(**click_kwargs)
@click.option(
    "--no-input",
    default=False,
    flag_value="no_input",
    help="Go with default options",
)
@click.option(
    "--write-post",
    default=True,
    flag_value="write_post",
    help="Write post to disk",
)
@click.option(
    "--open-editor",
    default=True,
    flag_value="open_editor",
    help="Open editor to continue writing",
)
def new(no_input, write_post, open_editor):
    from .blog_new import run

    os.chdir(here)

    env = Environment(
        loader=FileSystemLoader("templates"),
        autoescape=True,
    )

    app = run()
    ctx = app.context
    templates = {
        ext: env.get_template(f"post.{ext}.j2") for ext in app.template_filetypes
    }

    # TODO: find a way to choose tags as a list slice
    if isinstance(ctx["tags"], str):
        ctx["tags"] = (ctx["tags"],)

    template = templates[ctx["filetype"]]
    post = template.render(**ctx)

    filename = (
        here
        / "content"
        / "{}-{}.{}".format(date.today().isoformat(), ctx["slug"], ctx["filetype"])
    )

    if write_post:
        try:
            with open(filename, "x") as fp:
                fp.write(post)
        except FileExistsError as e:
            print(e)
            if not Confirm.ask("Continue as if nothing happened?", default=False):
                sys.exit()

    if open_editor:
        edit(filename)

    slug = ctx["slug"]
    status = ctx["status"]
    if github_pages() and Confirm.ask("Syndicate to Mastodon", default=True):
        if fedi_id := toot(ctx["slug"], ctx["summary"]):
            print("Inserting metadata FediID", fedi_id, " into ", filename)
            text = filename.read_text()
            if FEDI_ID_PLACEHOLDER not in text:
                print("Expected", FEDI_ID_PLACEHOLDER, "but not found")
            else:
                new_text = text.replace("__FEDI_ID_PLACEHOLDER__", fedi_id)
                filename.write_text(new_text)

                github_pages()

    git_prompt(filename, slug, status)

    # if Confirm.ask("Webmention bridgy to syndicate?", default= False):
    #     bridgy(slug, "mastodon")


def github_pages():
    if Confirm.ask("Publish post to Github Pages?", default=False):
        make_github = subprocess.run(["make", "github"])
        return make_github.returncode == 0
    else:
        return False


def toot(slug, summary, visiblity="public"):
    if not shutil.which("toot"):
        print("CLI toot not available!")
        return None

    text = f"""\
{summary}

{post_url(slug)}
#FluidQuest #blog
    """
    toot_output = subprocess.check_output(
        [
            "toot",
            "post",
            "-v",
            visiblity,
            "-u",
            f"{FEDIUSER}@{FEDIHOST}",
            "--no-color",
            text,
        ]
    ).decode("utf8")
    if match := re.search(r"\d+", toot_output):
        return match.group(0)
    else:
        print("Failed to parse FediID from toot output:", toot_output)
        return None


def git_prompt(filename, slug=None, status=None):
    if not slug:
        slug = Path(filename).stem

    if Confirm.ask("Track changes?", default=True):
        subprocess.run(["git", "add", filename])

    if Confirm.ask("New branch?", default=True):
        subprocess.run(["git", "switch", "--create", "write/" + slug])

    if Confirm.ask("Commit changes?", default=True):
        subprocess.run(["git", "commit", "-m", f'"Add post {slug}"'])

    if Confirm.ask("Push changes?", default=False):
        subprocess.run(["git", "push", "--set-upstream", "origin", "HEAD"])

    if Confirm.ask("Create a PR?", default=False):
        if shutil.which("gh"):
            # Do not prompt for title/body and just use commit info
            cmd = ["gh", "pr", "create", "--fill"]
            if status == "draft":
                cmd.append("--draft")
            subprocess.run(cmd)
        else:
            print("ERROR: This requires gh command. https://cli.github.com/manual/")


if __name__ == "__main__":
    os.chdir(here)
    write()
