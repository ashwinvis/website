---
Date: 2024-12-13T17:47:16.000Z
Tags: rsf
Summary: Original report by RSF
Syndication: https://mastodon.acc.sunet.se/@ashwinvis/113646766441125743
FediId: 113646766441125743
---
<p>Original report by <a href="https://mastodon.acc.sunet.se/tags/RSF" class="mention hashtag" rel="tag">#<span>RSF</span></a></p><p><a href="https://rsf.org/en/rsf-s-2024-round-journalism-suffers-exorbitant-human-cost-due-conflicts-and-repressive-regimes" target="_blank" rel="nofollow noopener noreferrer" translate="no"><span class="invisible">https://</span><span class="ellipsis">rsf.org/en/rsf-s-2024-round-jo</span><span class="invisible">urnalism-suffers-exorbitant-human-cost-due-conflicts-and-repressive-regimes</span></a></p><p>via <span class="h-card" translate="no"><a href="https://mastodon.social/@EndIsraeliApartheid" class="u-url mention">@<span>EndIsraeliApartheid</span></a></span></p><p><a href="https://mastodon.social/@EndIsraeliApartheid/113641032655165308" target="_blank" rel="nofollow noopener noreferrer" translate="no"><span class="invisible">https://</span><span class="ellipsis">mastodon.social/@EndIsraeliApa</span><span class="invisible">rtheid/113641032655165308</span></a></p>
