---
Date: 2024-06-21T19:22:13Z
Tags: ai
Summary: I get it. The whole AI as a term has garnered either blind adulation or hate due to aggressive corporate greed and misuse of the digital commons.
Syndication: https://mastodon.acc.sunet.se/@ashwinvis/112656235432036658
FediId: 112656235432036658
---

<p>I get it. The whole <a href="https://mastodon.acc.sunet.se/tags/AI" class="mention hashtag" rel="tag">#<span>AI</span></a> as a term has garnered either blind adulation or hate due to aggressive corporate greed and misuse of the digital commons. </p><p>Before you decide to get enrolled in to either of these camp, here is a kind reminder... </p><p>What you say AI, it is actually an umbrella term which may contain good applications, including scientific problems, for which there are no perfect models, yet.</p><p>What you are dismayed by / fascinated by is Generative AI based on images and text. That is it.</p>
