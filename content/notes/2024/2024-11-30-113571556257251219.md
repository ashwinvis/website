---
Date: 2024-11-30T11:00:20.000Z
Summary: [Solved] Correct me if I am wrong...
Syndication: https://mastodon.acc.sunet.se/@ashwinvis/113571556257251219
FediId: 113571556257251219
---
<p>[Solved] Correct me if I am wrong...</p><p>In Mastodon, you can search for hash tags and **follow** them, but there is no UI showing a list of followed hash tags?!</p><p>How will I recall all the hash tags that I have followed?</p>
