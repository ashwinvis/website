---
Date: 2024-08-11T16:05:45.000Z
Tags: lemmy,fediverse,eli5
Summary: Can some one ELI5 how moderation in Lemmy works? Who gets the reports? Is it the community moderator, user's instance admin or community's instance admin?
Syndication: https://mastodon.acc.sunet.se/@ashwinvis/112944240722596683
FediId: 112944240722596683
---
<p>Can some one <a href="https://mastodon.acc.sunet.se/tags/ELI5" class="mention hashtag" rel="tag">#<span>ELI5</span></a> how moderation in Lemmy works? Who gets the reports? Is it the community moderator, user&#39;s instance admin or community&#39;s instance admin?</p><p><a href="https://mastodon.acc.sunet.se/tags/fediverse" class="mention hashtag" rel="tag">#<span>fediverse</span></a> <a href="https://mastodon.acc.sunet.se/tags/lemmy" class="mention hashtag" rel="tag">#<span>lemmy</span></a></p>
