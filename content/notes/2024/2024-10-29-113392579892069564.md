---
Date: 2024-10-29T20:24:17.000Z
Tags: pandoc,beamer
Summary: An issue on pandoc for their beamer slides output that I filed while writing preparing for my Ph.D. dissertation ~ 5 years ago got fixed now. I actually forgot about this.
Syndication: https://mastodon.acc.sunet.se/@ashwinvis/113392579892069564
FediId: 113392579892069564
---
<p>An issue on <a href="https://mastodon.acc.sunet.se/tags/pandoc" class="mention hashtag" rel="tag">#<span>pandoc</span></a> for their <a href="https://mastodon.acc.sunet.se/tags/beamer" class="mention hashtag" rel="tag">#<span>beamer</span></a> slides output that I filed while writing preparing for my Ph.D. dissertation ~ 5 years ago got fixed now. I actually forgot about this.</p><p><a href="https://github.com/jgm/pandoc/issues/5769" target="_blank" rel="nofollow noopener noreferrer" translate="no"><span class="invisible">https://</span><span class="ellipsis">github.com/jgm/pandoc/issues/5</span><span class="invisible">769</span></a></p><p>My hunch and suggestion was right. Write good issues. It will be picked up at some point.</p>
