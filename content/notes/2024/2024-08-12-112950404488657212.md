---
Date: 2024-08-12T18:13:16.000Z
Summary: This is a very cool thread of vintage electronics!
Syndication: https://mastodon.acc.sunet.se/@ashwinvis/112950404488657212
FediId: 112950404488657212
---
<p>This is a very cool thread of vintage electronics!</p><p>&gt; <span class="h-card" translate="no"><a href="https://mastodon.social/@tubetime" class="u-url mention">@<span>tubetime</span></a></span> 🔗 <a href="https://mastodon.social/users/tubetime/statuses/112943565918836322" target="_blank" rel="nofollow noopener noreferrer" translate="no"><span class="invisible">https://</span><span class="ellipsis">mastodon.social/users/tubetime</span><span class="invisible">/statuses/112943565918836322</span></a></p>
