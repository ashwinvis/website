@ashwinvis
##########

:date: 2020-04-23
:image: {static}/images/avatar_ashwinvis.png
:badge: @ashwinvis
:status: draft

Here lies the notes conjured by @ashwinvis hailing from the social lands of
`mastodon.acc.sunet.se <https://mastodon.acc.sunet.se/@ashwinvis>`__,
`fediscience.org <https://fediscience.org/@ashwinvis>`__
and other forgotten lands.
