Contact
#######
:date: 2019-10-18
:modified: 2024-10-28
:slug: contact
:status: published
:html_header:
   <script language="JavaScript" type="text/javascript">
   \  function decrypt_email(a) {
   \    // ROT13 : a Caesar cipher
   \    // letter -> letter' such that code(letter') = (code(letter) + 13) modulo 26
   \    return a.replace(/[a-zA-Z]/g,
   \                     function(c){
   \             return String.fromCharCode((c <= "Z" ? 90 : 122) >= (c = c.charCodeAt(0) + 13) ? c : c - 26);
   \         })
   \  }
   \  // Generated using src/util.py
   \  function openPersonalMail(element) {
   \    var y = decrypt_email("znvygb:Nfujva Ivfuah Zbunana <nfujvaivf@cz.zr>");
   \    element.setAttribute("href", y);
   \  }
   \  function openWorkMail(element) {
   \    var y = decrypt_email("znvygb:Nfujva I. Zbunana <nfujva.zbunana@rappf.fr>");
   \    element.setAttribute("href", y);
   \  }
   \ </script>
   \ <link rel="stylesheet" href="https://unpkg.com/leaflet@1.9.4/dist/leaflet.css"
   \    integrity="sha256-p4NxAoJBhIIN+hmNHrzRCf9tD/miZyoHS5obTRR9BMY="
   \    crossorigin="" />
   \ <script src="https://unpkg.com/leaflet@1.9.4/dist/leaflet.js"
   \    integrity="sha256-20nQCchB9co0qIjJZRGuk2/Z9VM+kNiyxNV1lvTlZBo="
   \    crossorigin=""></script>

I would love to hear from you!
Drop me a message if you wish to talk to me or even leave a comment about my
research_ or software_ that I maintain.

.. raw:: html

   <table class="m-table m-flat m-big">
   <tbody>
     <tr>
         <div class="m-button m-info m-fullwidth" align-content="normal">
                   <div class="m-big"><b>Visiting / mailing address</b></div>
                   <span class="m-text m-small">
                       Isafjordsgatan 22,<br/>
                       164 40 Kista, Sweden
                   </span>
         </div>
     </tr>

     <tr>
     <td>
       <div class="m-button m-flat m-fullwidth">
         <a href="encrypted-personal-mail:Click to reveal" onclick="openPersonalMail(this);">
             <div class="m-big">Personal e-mail</div>
             <div class="m-small">Click to mail</div>
         </a>
       </div>
     </td>
     <td>
       <div class="m-button m-flat m-fullwidth">
         <a href="encrypted-work-mail:Click to reveal" onclick="openWorkMail(this);">
             <div class="m-big">Work e-mail</div>
             <div class="m-small">Click to mail</div>
         </a>
       </div>
     </td>
     </tr>

     </tbody>
     </table>

.. block-info:: Find directions to my office

    I try to be at the work most of the time, and I work from home occasionally.
    Let me know beforehand if you drop by.

    .. container:: m-row

        .. container:: m-col-l-10 m-push-l-1 m-col-m-7 m-nopadb

            .. raw:: html

                 <style>#map { height: 360px; }</style>
                 <div id="map"></div>
                 <script language="JavaScript" type="text/javascript" defer>

                    var map = L.map('map').setView([59.4049181, 17.9494329], 13);
                    var marker = L.marker([59.4049181, 17.9494329], {alt: "ENCCS / RISE"})
                       .addTo(map)
                       .bindPopup('ENCCS / RISE 🔗 <a href="https://duckduckgo.com/?t=ffab&q=Rise+Research+Institutes+Of+Sweden&iaxm=maps&bbox=17.948524106562004%2C59.405211928163574%2C17.950291048467292%2C59.40431260796924">DuckDuckGo ↗ </a>');


                    var popup = L.popup();

                    function onMapClick(e) {
                        popup
                            .setLatLng(e.latlng)
                            .setContent("You clicked the map at " + e.latlng.toString())
                            .openOn(map);
                    }

                    map.on('click', onMapClick);

                    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
                       { maxZoom: 19, attribution: '© OpenStreetMap' }
                    ).addTo(map);
                 </script>
                 <noscript>
                    <p>
                      SMHI 🔗
                      <a href="https://www.qwant.com/maps/place/osm:node:4743840719@Sveriges_meteorologiska_och_hydrologiska_institut#map=15.26/58.5795811/16.1459828">
                        Qwant Maps ↗
                      </a>
                    </p>
                 </noscript>


You can also reach me via social media accounts listed in the footer.

.. _research: /pages/research
.. _software: /pages/software
